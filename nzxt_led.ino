    #include <Adafruit_NeoPixel.h>
     
    #define PIN      6
    #define N_LEDS 10
     
    Adafruit_NeoPixel strip = Adafruit_NeoPixel(N_LEDS, PIN, NEO_GRB + NEO_KHZ800);
     
    void setup() {
      strip.begin();
      strip.setBrightness(255);
    }
     
    void loop() {
      //chase(strip.Color(0, 0, 0));
      //chase(strip.Color(0, 0, 240)); 
      chase(strip.Color(0, 0, 250)); // Blue
    }
     
    static void chase(uint32_t c) {
      for(uint16_t i=0; i<strip.numPixels()+4; i++) {
          strip.setPixelColor(i  , c); // Draw new pixel
          strip.show();
          delay(0);
      }
    }
